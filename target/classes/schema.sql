DROP TABLE IF EXISTS TRANSACTIONS;

CREATE TABLE TRANSACTIONS (
  ID LONG AUTO_INCREMENT PRIMARY KEY,
  AMOUNT DECIMAL NOT NULL,
  TYPE VARCHAR(20) NOT NULL,
  USERID LONG NOT NULL,
  EVENTTIME TIMESTAMP NOT NULL
);