INSERT INTO TRANSACTIONS (AMOUNT, TYPE, USERID, EVENTTIME) VALUES
(25, 'deposit', 1, current_timestamp ),
(-5, 'withdraw', 1, current_timestamp),
(10, 'deposit', 2, '2019-01-01 12:34:56.789'),
(-2.52, 'withdraw', 2, '2018-12-01 10:34:56.789');