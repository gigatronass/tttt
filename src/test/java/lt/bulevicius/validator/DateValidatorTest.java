package lt.bulevicius.validator;

import org.junit.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DateValidatorTest {

    private static final Timestamp TIMESTAMP_FROM = Timestamp.valueOf("2020-03-03 11:11:11.123");
    private static final Timestamp TIMESTAMP_TO = Timestamp.valueOf("2020-08-08 11:11:11.123");

    @Test
    public void failsWhenDateFromIsGreaterThanDateTo() {
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> {
                    new DateValidator().checkIsPeriodValid(TIMESTAMP_TO, TIMESTAMP_FROM);
                }
        );

        assertEquals("Date from is after date to", exception.getMessage());
    }
}
