package lt.bulevicius.service;

import lt.bulevicius.model.TransactionEntity;
import lt.bulevicius.repository.TransactionRepository;
import lt.bulevicius.serviceimpl.TransactionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static lt.bulevicius.TransactionType.DEPOSIT;
import static lt.bulevicius.TransactionType.WITHDRAW;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TransactionServiceTest {

    private static final Timestamp TIMESTAMP_FROM = Timestamp.valueOf("2019-01-01 11:11:11.123");
    private static final Timestamp TIMESTAMP_TO = Timestamp.valueOf("2020-02-02 11:11:11.123");
    private static final Timestamp TRANSACTION_TIMESTAMP = Timestamp.valueOf("2019-12-12 12:12:12.123");

    @InjectMocks
    TransactionServiceImpl transactionService;
    @Mock
    TransactionRepository repository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void returnsCorrectBalance() {
        when(repository.total(123L,3L)).thenReturn(new BigDecimal(2));
        when(repository.existsByUserId(any())).thenReturn(true);
        when(repository.existsByAccountIdAndUserId(any(), any())).thenReturn(true);
        Assertions.assertEquals(new BigDecimal(2), transactionService.getBalance(123L, 3L));
    }

    @Test
    public void createsDepositTransaction() {
        when(repository.existsByUserId(any())).thenReturn(true);
        when(repository.existsByAccountIdAndUserId(any(), any())).thenReturn(true);
        Assertions.assertEquals(createsTransactionEntity(), transactionService.createTransaction(WITHDRAW, new BigDecimal(3), 123L,4L));
    }

    @Test
    public void createsWithdrawTransaction() {
        Assertions.assertEquals(createsTransactionEntity(), transactionService.createTransaction(DEPOSIT, new BigDecimal(51), 123L,2L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenTakesMoreThanAccountBalance() {
        Assertions.assertEquals(createsTransactionEntity(), transactionService.createTransaction(WITHDRAW, new BigDecimal(15), 123L,4L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenDepositAmountIsNegative() {
        Assertions.assertEquals(createsTransactionEntity(), transactionService.createTransaction(DEPOSIT, new BigDecimal(-25), 123L,4L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenDepositAmountIsZero() {
        Assertions.assertEquals(createsTransactionEntity(), transactionService.createTransaction(DEPOSIT, new BigDecimal(0), 123L,4L));
    }

    @Test
    public void returnsTransactionsByPeriod() {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setId(1L);
        transactionEntity.setUserId(4L);
        transactionEntity.setAmount(new BigDecimal(3));
        transactionEntity.setType(WITHDRAW);
        transactionEntity.setEventTime(TRANSACTION_TIMESTAMP);
        List<TransactionEntity> transactions = new ArrayList<>();
        transactions.add(transactionEntity);

        when(repository.existsByUserId(any())).thenReturn(true);
        when(repository.findAllByEventTimeBetweenAndAccountIdAndUserId(TIMESTAMP_FROM, TIMESTAMP_TO, 123L, 4L)).thenReturn(transactions);

        Assertions.assertEquals(transactions, transactionService.getTransactionsByPeriod(TIMESTAMP_FROM, TIMESTAMP_TO, 123L,4L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenIncorrectPeriod() {
        transactionService.getTransactionsByPeriod(TIMESTAMP_TO, TIMESTAMP_FROM, 123L,4L);
    }

    private TransactionEntity createsTransactionEntity() {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setUserId(4L);
        transactionEntity.setAmount(new BigDecimal(3));
        transactionEntity.setType(WITHDRAW);
        transactionEntity.setEventTime(TRANSACTION_TIMESTAMP);
        when(repository.total(123L,4L)).thenReturn(new BigDecimal(10));
        when(repository.save(any())).thenReturn(transactionEntity);
        return transactionEntity;
    }
}
