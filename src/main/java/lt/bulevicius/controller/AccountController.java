package lt.bulevicius.controller;

import lt.bulevicius.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    AccountService accountService;

    @RequestMapping("createAccount/{userId}/{accountId}")
    public String createAccount(@PathVariable Long userId, @PathVariable Long accountId) {
        accountService.createAccount(userId, accountId);
        return "Account " + accountId + " succesfully created";
    }
}