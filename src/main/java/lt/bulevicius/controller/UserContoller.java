package lt.bulevicius.controller;

import lt.bulevicius.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserContoller {

    @Autowired
    UserService userService;

    @RequestMapping("createUser/{userId}")
    public String createUser(@PathVariable Long userId) {
        userService.createUser(userId);
        return "User " + userId + " succesfully created";
    }
}