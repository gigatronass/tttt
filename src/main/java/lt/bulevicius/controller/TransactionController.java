package lt.bulevicius.controller;

import lt.bulevicius.model.TransactionEntity;
import lt.bulevicius.service.AccountService;
import lt.bulevicius.service.TransactionService;
import lt.bulevicius.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import static lt.bulevicius.TransactionType.DEPOSIT;
import static lt.bulevicius.TransactionType.WITHDRAW;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @RequestMapping("balance/{accountId}/{userId}")
    public BigDecimal getBalanceByAccount(@PathVariable Long accountId, @PathVariable Long userId) {
        return transactionService.getBalance(accountId, userId);
    }

    @RequestMapping("deposit/{amount}/{accountId}/{userId}")
    public String createDeposit(@PathVariable BigDecimal amount, @PathVariable Long accountId, @PathVariable Long userId) {
        transactionService.createTransaction(DEPOSIT, amount, accountId, userId);
        return "Succesfully deposit " + amount;
    }

    @RequestMapping("withdraw/{amount}/{accountId}/{userId}")
    public String createEmployee(@PathVariable BigDecimal amount, @PathVariable Long accountId, @PathVariable Long userId) {
        transactionService.createTransaction(WITHDRAW, amount.negate(), accountId, userId);
        return "Succesfully withdraw " + amount.negate();
    }

    @GetMapping("/{periodFrom}/{periodTo}/{accountId}/{userId}")
    public ResponseEntity<TransactionEntity> getTransactionsByPeriod(@PathVariable("periodFrom") Timestamp periodFrom, @PathVariable("periodTo") Timestamp periodTo, @PathVariable Long accountId, @PathVariable Long userId) {
        List<TransactionEntity> entity = transactionService.getTransactionsByPeriod(periodFrom, periodTo, accountId, userId);
        return new ResponseEntity(entity, new HttpHeaders(), HttpStatus.OK);
    }
}