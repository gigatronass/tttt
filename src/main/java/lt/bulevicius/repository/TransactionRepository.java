package lt.bulevicius.repository;

import lt.bulevicius.model.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Repository()
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {

    @Query("SELECT SUM(m.amount) FROM TransactionEntity m where m.accountId = ?1 and m.userId = ?2")
    BigDecimal total(Long accountId, Long userId);

    //List<TransactionEntity> findAllByEventTimeBetweenAndAccountIdAndUserId(Timestamp periodFrom, Timestamp periodTo, Long accountId, Long userId);
    //@Query("SELECT m.id, m.amount, m.type, m.accountId, m.userId, m.eventTime FROM TransactionEntity m where (m.eventTime between ?1 and ?2) and m.accountId = ?3 and m.userId = ?4")
    //http://localhost:8081/transactions/2018-01-01%2000:00:01.467/2021-03-01%2000:00:01.851/321/2
    List<TransactionEntity> findAllByEventTimeBetweenAndAccountIdAndUserId(Timestamp periodFrom, Timestamp periodTo, Long accountId, Long userId);

    boolean existsByUserId(Long userId);

    boolean existsByAccountIdAndUserId(Long accountId, Long userId);
}