package lt.bulevicius.service;

import lt.bulevicius.model.AccountEntity;

public interface AccountService {

    AccountEntity createAccount(Long userId, Long accountId);
}
