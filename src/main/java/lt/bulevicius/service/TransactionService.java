package lt.bulevicius.service;

import lt.bulevicius.TransactionType;
import lt.bulevicius.model.TransactionEntity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public interface TransactionService {

    BigDecimal getBalance(Long accountId, Long userId);
    TransactionEntity createTransaction(TransactionType transactionType, BigDecimal amount, Long accountId, Long userId);
    List<TransactionEntity> getTransactionsByPeriod(Timestamp periodFrom, Timestamp periodTo, Long accountId, Long userId);
}