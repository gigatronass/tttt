package lt.bulevicius.service;

import lt.bulevicius.model.UserEntity;

public interface UserService {

    UserEntity createUser(Long userId);
}
