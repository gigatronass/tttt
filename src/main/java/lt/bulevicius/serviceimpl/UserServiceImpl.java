package lt.bulevicius.serviceimpl;

import lt.bulevicius.model.UserEntity;
import lt.bulevicius.repository.UserRepository;
import lt.bulevicius.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;

    public UserEntity createUser(Long userId) {
        if (repository.existsByUserId(userId)) {
            throw new IllegalArgumentException("This user already exists");
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(userId);
        userEntity.setEventTime(new Timestamp(System.currentTimeMillis()));
        UserEntity entity = repository.save(userEntity);
        return entity;
    }
}