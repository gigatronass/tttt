package lt.bulevicius.serviceimpl;

import lt.bulevicius.model.AccountEntity;
import lt.bulevicius.repository.AccountRepository;
import lt.bulevicius.repository.UserRepository;
import lt.bulevicius.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository repository;
    @Autowired
    UserRepository userRepository;

    public AccountEntity createAccount(Long userId, Long accountId) {
        checksForValidUserAccount(userId, accountId);
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountId(accountId);
        accountEntity.setUserId(userId);
        accountEntity.setEventTime(new Timestamp(System.currentTimeMillis()));
        AccountEntity entity = repository.save(accountEntity);
        return entity;
    }

    private void checksForValidUserAccount(Long userId, Long accountId) {
        if (!userRepository.existsByUserId(userId)) {
            throw new IllegalArgumentException("User is not exists");
        }
        if (repository.existsByUserIdAndAccountId(userId, accountId)) {
            throw new IllegalArgumentException("This account already exists");
        }
    }
}
