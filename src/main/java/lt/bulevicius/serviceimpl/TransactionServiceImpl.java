package lt.bulevicius.serviceimpl;

import lt.bulevicius.TransactionType;
import lt.bulevicius.model.TransactionEntity;
import lt.bulevicius.repository.TransactionRepository;
import lt.bulevicius.service.TransactionService;
import lt.bulevicius.validator.DateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static lt.bulevicius.TransactionType.DEPOSIT;
import static lt.bulevicius.TransactionType.WITHDRAW;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository repository;

    public BigDecimal getBalance(Long accountId, Long userId) {
        isExistsUserId(userId);
        isExistsAccountForUser(accountId, userId);
        return repository.total(accountId, userId);
    }

    public TransactionEntity createTransaction(TransactionType transactionType, BigDecimal amount, Long accountId, Long userId) {
        isExistsUserId(userId);
        isExistsAccountForUser(accountId, userId);
        if (transactionType == DEPOSIT) {
            isLessOrEqual(amount, new BigDecimal(0));
        }
        if (transactionType == WITHDRAW && getBalance(accountId, userId).compareTo(amount.abs()) < 0) {
            throw new IllegalArgumentException("Can't withdraw more than account balance");
        }
        TransactionEntity transaction = new TransactionEntity();
        transaction.setAmount(amount);
        transaction.setType(transactionType);
        transaction.setAccountId(accountId);
        transaction.setUserId(userId);
        transaction.setEventTime(new Timestamp(System.currentTimeMillis()));
        TransactionEntity entity = repository.save(transaction);
        return entity;
    }

    public List<TransactionEntity> getTransactionsByPeriod(Timestamp periodFrom, Timestamp periodTo, Long accountId, Long userId) {
        isExistsUserId(userId);
        isExistsAccountForUser(accountId, userId);
        new DateValidator().checkIsPeriodValid(periodFrom, periodTo);
        List<TransactionEntity> employeeList = repository.findAllByEventTimeBetweenAndAccountIdAndUserId(periodFrom, periodTo, accountId, userId);
        return employeeList.size() > 0 ? employeeList : new ArrayList<>();
    }

    private void isLessOrEqual(BigDecimal amount, BigDecimal bigDecimal) {
        if (amount.compareTo(bigDecimal) <= 0 ){
            throw new IllegalArgumentException("Can't deposit negative amount or 0");
        }
    }

    private boolean isExistsUserId(Long userId) {
        if (!repository.existsByUserId(userId)) {
            throw new IllegalArgumentException("User is not exists");
        }
        return true;
    }

    private boolean isExistsAccountForUser(Long accountId, Long userId) {
        if (!repository.existsByAccountIdAndUserId(accountId, userId)) {
            throw new IllegalArgumentException("Selected account for user is not exists");
        }
        return true;
    }
}