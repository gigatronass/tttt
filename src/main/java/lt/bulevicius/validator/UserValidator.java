package lt.bulevicius.validator;

import lt.bulevicius.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UserValidator {

    @Autowired
    TransactionRepository repository;

    public boolean isExistsUserId(Long userId) {
        if (!repository.existsByUserId(userId)) {
            throw new IllegalArgumentException("User is not exists");
        }
        return true;
    }
}
