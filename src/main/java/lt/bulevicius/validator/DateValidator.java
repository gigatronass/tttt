package lt.bulevicius.validator;

import java.sql.Timestamp;

public class DateValidator {

    public void checkIsPeriodValid(Timestamp dateFrom, Timestamp dateTo) {
        if (dateTo.before(dateFrom)) {
            throw new IllegalArgumentException("Date from is after date to");
        }
    }
}
